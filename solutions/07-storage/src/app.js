const firebaseConfig = {
    //your firebase config here
};
firebase.initializeApp(firebaseConfig);

async function signIn(ev) {
    ev.preventDefault();
    try {
        firebase.auth().useDeviceLanguage(); //or; .languageCode = 'sv';
        const provider = new firebase.auth.GoogleAuthProvider();
        provider.addScope('profile');
        provider.addScope('email');
        const result = await firebase.auth().signInWithPopup(provider);
        const user = result.user;
        console.info('signed in', user);
    } catch (err) {
        console.error(`*** ${err.message} (${err.code})`);
    }
}

async function signOut(ev) {
    ev.preventDefault();
    try {
        await firebase.auth().signOut();
        console.log('signed out');
    } catch (err) {
        alert(`Failed to logout: ${err.message}`);
        console.error('**', err);
    }
}

function toggleVisibility(isAuth) {
    const unauth = document.querySelector('header .unauth');
    const auth = document.querySelector('header .auth');

    if (isAuth) {
        auth.classList.remove('hidden');
        auth.classList.add('visible');
        unauth.classList.remove('visible');
        unauth.classList.add('hidden');
    } else {
        auth.classList.remove('visible');
        auth.classList.add('hidden');
        unauth.classList.remove('hidden');
        unauth.classList.add('visible');
    }
}

function updateAuthState(user) {
    if (user) {
        const userName = document.querySelector('#userName');
        userName.innerHTML = user.displayName;
        toggleVisibility(true);
    } else {
        toggleVisibility(false);
    }
}


//------------------------------
async function uploadImage(ev) {
    ev.preventDefault();

    const file = ev.target.files[0];
    console.debug('* file:', file);

    const ref = firebase.storage().ref('files/' + file.name);
    const task = ref.put(file);
    const progressbar = document.querySelector('.upload progress');
    task.on(firebase.storage.TaskEvent.STATE_CHANGED,
        (snap) => {
            const percent = 100 * (snap.bytesTransferred / snap.totalBytes);
            console.info('* progress', percent + '%', snap.bytesTransferred + 'bytes');
            progressbar.value = percent;
        },
        (err) => {
            console.error('** failed:', err);
        },
        async () => {
            const url = await task.snapshot.ref.getDownloadURL();
            console.info('URL:', url);

            const img = document.createElement('img');
            img.src = url;
            const oneImage = document.querySelector('.oneImage');
            oneImage.appendChild(img);
        }
    );
}

async function listImages(ev) {

}

// DRY
['signIn', 'signOut', 'listImages']
    .forEach(action => {
        const elem = document.querySelector('#' + action);
        elem.addEventListener('click', eval(action));
    });

document.querySelector('#uploadImage')
    .addEventListener('change', uploadImage);

firebase.auth()
    .onAuthStateChanged(updateAuthState);
