const firebaseConfig = {
    //your firebase config here
};

firebase.initializeApp(firebaseConfig);
const db = firebase.firestore();
const personsCol = db.collection('persons');


async function createPerson(ev) {
    ev.preventDefault();
    const name = document.querySelector('#name').value;
    const age  = document.querySelector('#age').value;
    console.log('**', 'payload', name, age);

    const doc   = await personsCol.add({name, age});
    console.log('**', 'created', doc.id);
}

async function deletePerson(ev) {
    ev.preventDefault();
    const id = ev.target.dataset.id;
    console.log('**', 'id', id);

    await personsCol.doc(id).delete();
    console.log('**', 'deleted', id);
}

async function personsHandler(snap) {
    const rows = document.querySelector('#rows');
    rows.innerHTML= '';
    snap.forEach(doc => {
        let id = doc.id;
        let data = doc.data();
        console.log('**', id, ':', data);
        let tr = document.createElement('tr');
        tr.innerHTML = `
            <td>${id}</td>
            <td>${data.name}</td>
            <td>${data.age}</td>
            <td><button data-id="${id}" class="delete">Delete</button></td>
            `;
        rows.appendChild(tr);
        tr.querySelector('button.delete').addEventListener('click', deletePerson);
    });
}

personsCol.onSnapshot(personsHandler);
document.querySelector('#create').addEventListener('click', createPerson);

