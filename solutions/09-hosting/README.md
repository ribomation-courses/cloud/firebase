# Firestore Demo

## Preparation
First obtain your Firebase project-id and
insert it in the following files

* `./.firebaserc`
* `./src/app.js`

## Run by Emulators
First start the emulators instances.
N.B. the Firestore emulator requires `java` to be installed.

    npm run emulate

Then open a browser to http://localhost:5000/

## Deploy
Deploy to Firebase

    npm run deploy

Inspect the output, it should show you the URL to your hosted app.
Then open a browser to it.

If your project-id is `123456789`, then your hosting URL would be
something like
https://123456789.firebaseapp.com/

