
const PROJECT = 'your own project-id';
const REGION = 'us-central1';
const FUNCTION = 'messagesIn';
const OUTPUT = 'messagesOut';

const isEmulator = location.hostname === 'localhost';

const baseUrl = isEmulator
    ? `http://localhost:5001/${PROJECT}/${REGION}/${FUNCTION}`
    : `https://${REGION}-${PROJECT}.cloudfunctions.net/${FUNCTION}`
    ;

const db = firebase.firestore();
if (isEmulator) {
    db.settings({
        host: "localhost:8081",
        ssl: false
    });
}

let unsubscribe;

['signIn', 'signOut', 'sendMessage']
    .forEach(action => {
        const elem = document.querySelector('#' + action);
        elem.addEventListener('click', eval(action));
    });

firebase.auth()
    .onAuthStateChanged(updateAuthState);


async function messagesOutListener(result) {
    const rows = document.querySelector('#rows');
    rows.innerHTML = '';
    result.forEach(doc => {
        const id = doc.id;
        const data = doc.data();
        let tr = document.createElement('tr');
        tr.innerHTML = `
            <td>${id}</td>
            <td>${data.message}</td>
            <td>${data.created.toDate().toLocaleString()}</td>
            `;
        rows.appendChild(tr);
    });
}

async function sendMessage(ev) {
    const messageText = document.querySelector('#messageText');
    const msg = messageText.value;
    if (msg) {
        const url = `${baseUrl}?msg=${msg}`;
        const response = await fetch(url);
        console.log('response:', response);
        messageText.value = '';
    }
}

function updateAuthState(user) {
    if (user) {
        const userName = document.querySelector('#userName');
        userName.innerHTML = user.displayName;
        toggleVisibility(true);
        unsubscribe = db.collection(OUTPUT)
            .orderBy('created', 'desc')
            .onSnapshot(messagesOutListener);
    } else {
        toggleVisibility(false);
        if (unsubscribe) {
            unsubscribe();
            unsubscribe = undefined;
        }
    }
}

async function signIn(ev) {
    ev.preventDefault();
    try {
        firebase.auth().useDeviceLanguage();
        const provider = new firebase.auth.GoogleAuthProvider();
        provider.addScope('profile');
        provider.addScope('email');
        const result = await firebase.auth().signInWithPopup(provider);
        const user = result.user;
        console.info('signed in', user.email);
    } catch (err) {
        console.error(`*** (${err.code}) ${err.message}`);
        alert(err.message);
    }
}

async function signOut(ev) {
    ev.preventDefault();
    try {
        await firebase.auth().signOut();
    } catch (err) {
        alert(`Failed to logout: ${err.message}`);
        console.error('**', err);
        alert(err.message);
    }
}

function toggleVisibility(isAuth) {
    if (isAuth) {
        document.querySelectorAll('.ifAnon').forEach(elem => elem.classList.add('hidden'));
        document.querySelectorAll('.ifAuth').forEach(elem => elem.classList.remove('hidden'));
    } else {
        document.querySelectorAll('.ifAnon').forEach(elem => elem.classList.remove('hidden'));
        document.querySelectorAll('.ifAuth').forEach(elem => elem.classList.add('hidden'));
    }
}
