const fn = require('firebase-functions');
const adm = require('firebase-admin');
adm.initializeApp();

const INPUT = 'messagesIn';
const OUTPUT = 'messagesOut';


exports.messagesIn = fn.https
    .onRequest(async (req, res) => {
        const msg = req.query.msg || 'empty message';
        console.info('*** msg:', msg);

        await adm.firestore().collection(INPUT).add({
            message: msg,
            created: new Date()
        });

        res.set({
            'Content-Type': 'text/plain',
            'Access-Control-Allow-Origin': '*', //CORS hdr
        });

        res.status(200).send('RECV: ' + msg);
    });


exports.messagesOut = fn.firestore
    .document('messagesIn/{msgId}')
    .onCreate(async (snap, ctx) => {
        const msgId = ctx.params.msgId;
        console.info('*** msgId:', msgId);

        const data = snap.data()
        console.info('*** message:', data.message);
        console.info('*** created:', data.created);

        return adm.firestore().collection(OUTPUT).add({
            message: data.message.toUpperCase(),
            created: new Date()
        });
    });

