const firebaseConfig = {
    //your firebase config here
};
firebase.initializeApp(firebaseConfig);
const db = firebase.firestore();
let unsubscribe;
let currentId;


async function personsHandler(snap) {
    const rows = document.querySelector('#rows');
    rows.innerHTML = '';
    snap.forEach(doc => {
        let id = doc.id;
        let data = doc.data();
        console.log('**', id, ':', data);

        let tr = document.createElement('tr');
        tr.innerHTML = `
            <td>${id}</td>
            <td>${data.name}</td>
            <td>${data.age}</td>
            <td>
                <button data-id="${id}" class="edit">Edit</button>
                <button data-id="${id}" class="delete">Delete</button>
            </td>
            `;
        rows.appendChild(tr);
        tr.querySelector('button.edit').addEventListener('click', editPerson);
        tr.querySelector('button.delete').addEventListener('click', deletePerson);
    });
}

async function savePerson(ev) {
    ev.preventDefault();
    const name = document.querySelector('#name');
    const age = document.querySelector('#age');
    const payload = { 
        name: name.value,
        age: age.value
     };

    if (currentId) {
        await db.collection('persons').doc(currentId).update(payload);
        console.log('**', 'update',currentId);
        name.value = age.value = '';
        currentId = undefined;
    } else {
        const doc = await db.collection('persons').add(payload);
        console.log('**', 'created', doc.id);
        name.value = age.value = '';
        currentId = undefined;
    }
}

async function deletePerson(ev) {
    ev.preventDefault();
    const id = ev.target.dataset.id;
    console.log('**', 'id', id);

    await db.collection('persons').doc(id).delete();
    console.log('**', 'deleted', id);
}

async function editPerson(ev) {
    ev.preventDefault();

    const id = ev.target.dataset.id;
    const doc = await db.collection('persons').doc(id).get();
    const data = doc.data();

    const name = document.querySelector('#name');
    const age = document.querySelector('#age');
    name.value = data.name;
    age.value = data.age;
    currentId = id;
}




async function signInByGoogle(ev) {
    ev.preventDefault();
    try {
        firebase.auth().useDeviceLanguage(); //or; .languageCode = 'sv';
        const provider = new firebase.auth.GoogleAuthProvider();
        provider.addScope('profile');
        provider.addScope('email');
        const result = await firebase.auth().signInWithPopup(provider);
        const user = result.user;
        console.info('signed in', user);
    } catch (err) {
        console.error(`*** ${err.message} (${err.code})`);
    }
}

async function signOut(ev) {
    ev.preventDefault();
    try {
        await firebase.auth().signOut();
        console.log('signed out');
    } catch (err) {
        alert(`Failed to logout: ${err.message}`);
        console.error('**', err);
    }
}

function toggleVisibility(isAuth) {
    const unauth = document.querySelector('header .unauth');
    const auth = document.querySelector('header .auth');
    const tbl = document.querySelector('#tbl');

    if (isAuth) {
        auth.classList.remove('hidden');
        auth.classList.add('visible');
        unauth.classList.remove('visible');
        unauth.classList.add('hidden');
        tbl.classList.remove('hidden');
        tbl.classList.add('visible');
    } else {
        auth.classList.remove('visible');
        auth.classList.add('hidden');
        unauth.classList.remove('hidden');
        unauth.classList.add('visible');
        tbl.classList.add('hidden');
        tbl.classList.remove('visible');
    }
}

function updateAuthState(user) {
    if (user) {
        const userName = document.querySelector('#userName');
        userName.innerHTML = user.displayName;
        toggleVisibility(true);

        unsubscribe = db.collection('persons')
            .onSnapshot(personsHandler);
    } else {
        toggleVisibility(false);

        if (unsubscribe) {
            unsubscribe();
            unsubscribe = undefined;
        }
    }
}

document.querySelector('#signIn')
    .addEventListener('click', signInByGoogle);

document.querySelector('#signOut')
    .addEventListener('click', signOut);

document.querySelector('#create')
    .addEventListener('click', savePerson);

firebase.auth()
    .onAuthStateChanged(updateAuthState);


