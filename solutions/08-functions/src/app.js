const PROJECT  = 'your project id';
const REGION   = 'us-central1';
const FUNCTION = 'messagesIn';
const URL      = `https://${REGION}-${PROJECT}.cloudfunctions.net/${FUNCTION}`;
const OUTPUT   = 'messagesOut';

const firebaseConfig = {
    projectId: PROJECT,
    apiKey: "your api key",
    authDomain: `${PROJECT}.firebaseapp.com`,
};
firebase.initializeApp(firebaseConfig);


async function messagesOutListener(result) {
    const rows = document.querySelector('#rows');
    rows.innerHTML = '';
    result.forEach(doc => {
        const id = doc.id;
        const data = doc.data();
        let tr = document.createElement('tr');
        tr.innerHTML = `
            <td>${id}</td>
            <td>${data.message}</td>
            <td>${data.created.toDate().toLocaleString()}</td>
            `;
        rows.appendChild(tr);
    });
}

async function sendMessage(ev) {
    const messageText = document.querySelector('#messageText');
    const msg = messageText.value;
    if (msg) {
        const url = `${URL}?msg=${msg}`;
        const response = await fetch(url);
        console.log('response:', response);
        messageText.value = '';
    }
}

['signIn', 'signOut', 'sendMessage']
    .forEach(action => {
        const elem = document.querySelector('#' + action);
        elem.addEventListener('click', eval(action));
    });

firebase.auth()
    .onAuthStateChanged(updateAuthState);



let unsubscribe;
function updateAuthState(user) {
    if (user) {
        const userName = document.querySelector('#userName');
        userName.innerHTML = user.displayName;
        toggleVisibility(true);
        unsubscribe = firebase.firestore()
            .collection(OUTPUT)
            .orderBy('created', 'desc')
            .onSnapshot(messagesOutListener);
    } else {
        toggleVisibility(false);
        if (unsubscribe) {
            unsubscribe();
            unsubscribe = undefined;
        }
    }
}

async function signIn(ev) {
    ev.preventDefault();
    try {
        firebase.auth().useDeviceLanguage(); //or; .languageCode = 'sv';
        const provider = new firebase.auth.GoogleAuthProvider();
        provider.addScope('profile');
        provider.addScope('email');
        const result = await firebase.auth().signInWithPopup(provider);
        const user = result.user;
        console.info('signed in', user);
    } catch (err) {
        console.error(`*** ${err.message} (${err.code})`);
    }
}

async function signOut(ev) {
    ev.preventDefault();
    try {
        await firebase.auth().signOut();
        console.log('signed out');
    } catch (err) {
        alert(`Failed to logout: ${err.message}`);
        console.error('**', err);
    }
}

function toggleVisibility(isAuth) {
    const unauth = document.querySelector('header .unauth');
    const auth = document.querySelector('header .auth');

    if (isAuth) {
        auth.classList.remove('hidden');
        auth.classList.add('visible');
        unauth.classList.remove('visible');
        unauth.classList.add('hidden');
    } else {
        auth.classList.remove('visible');
        auth.classList.add('hidden');
        unauth.classList.remove('hidden');
        unauth.classList.add('visible');
    }
}



//------------------------------
