const PATH = require('path');
const FS = require('fs');

function readFileByPromise(filename) {
    return new Promise((success, failure) => {
        FS.readFile(filename, 'utf8', (err, data) => {
            if (err) { failure(err); }
            else { success(data); }
        });
    });
}

async function readFileAsync(filename) {
    try {
        const result = await readFileByPromise(filename);
        console.info(result.toUpperCase());
    } catch (err) {
        console.error('***', err);
    }
}

const filename = process.argv[2] || PATH.basename(__filename);
console.info('filename', filename);
readFileAsync(filename);
// readFileByPromise(filename).then((content) => {
//     console.info(content.toUpperCase());
// })