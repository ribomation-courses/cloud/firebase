const FS = require('fs');
const PATH = require('path');

function readByPromise(filename) {
    return new Promise((success, failure) => {
        FS.readFile(filename, (err, data) => {
            if (err) {
                console.debug('** fail', err);
                failure(err);
            } else {
                console.debug('** succ', data.length, 'bytes');
                success(data.toString());
            }
        });
    });
}

async function readFileAsync(filename) {
    try {
        const result = await readByPromise(filename);
        console.info('result', result.toUpperCase());
    } catch (err) {
        console.error('cannot open/read ' + filename, err);
    }
}

const filename = process.argv[2] || PATH.basename(__filename);
console.info('filename', filename);
readFileAsync(filename);
