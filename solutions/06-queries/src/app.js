const firebaseConfig = {
    //your firebase config here
};
firebase.initializeApp(firebaseConfig);
const db = firebase.firestore();


function populate(result) {
    const rows = document.querySelector('#rows');
    rows.innerHTML = '';
    result.forEach(doc => {
        const id = doc.id;
        const data = doc.data();
        let tr = document.createElement('tr');
        tr.innerHTML = `
            <td>${id}</td>
            <td>${data.name}</td>
            <td>${data.age}</td>
            <td>${data.postcode}</td>
            <td>${data.city}</td>
            `;
        rows.appendChild(tr);
    });
}

async function byPostcode(ev) {
    ev.preventDefault();
    try {
        const result = await db.collection('persondata')
            .orderBy('postcode')
            .get();
        populate(result);
    } catch (err) {
        console.error(`*** ${err.message} (${err.code})`);
    }
}

async function inStockholm(ev) {
    ev.preventDefault();
    try {
        const result = await db.collection('persondata')
            .where('city', '==', 'stockholm')
            .orderBy('postcode')
            .get();
        populate(result);
    } catch (err) {
        console.error(`*** ${err.message} (${err.code})`);
    }
}

async function olderThan(ev) {
    ev.preventDefault();
    try {
        const result = await db.collection('persondata')
            .where('age', '>=', 42)
            .orderBy('age', 'desc')
            .get();
        populate(result);
    } catch (err) {
        console.error(`*** ${err.message} (${err.code})`);
    }
}



async function signIn(ev) {
    ev.preventDefault();
    try {
        firebase.auth().useDeviceLanguage(); //or; .languageCode = 'sv';
        const provider = new firebase.auth.GoogleAuthProvider();
        provider.addScope('profile');
        provider.addScope('email');
        const result = await firebase.auth().signInWithPopup(provider);
        const user = result.user;
        console.info('signed in', user);
    } catch (err) {
        console.error(`*** ${err.message} (${err.code})`);
    }
}

async function signOut(ev) {
    ev.preventDefault();
    try {
        await firebase.auth().signOut();
        console.log('signed out');
    } catch (err) {
        alert(`Failed to logout: ${err.message}`);
        console.error('**', err);
    }
}

function toggleVisibility(isAuth) {
    const unauth = document.querySelector('header .unauth');
    const auth = document.querySelector('header .auth');

    if (isAuth) {
        auth.classList.remove('hidden');
        auth.classList.add('visible');
        unauth.classList.remove('visible');
        unauth.classList.add('hidden');
    } else {
        auth.classList.remove('visible');
        auth.classList.add('hidden');
        unauth.classList.remove('hidden');
        unauth.classList.add('visible');
    }
}

function updateAuthState(user) {
    if (user) {
        const userName = document.querySelector('#userName');
        userName.innerHTML = user.displayName;
        toggleVisibility(true);
    } else {
        toggleVisibility(false);
    }
}

// DRY
['signIn', 'signOut', 'byPostcode', 'inStockholm', 'olderThan']
    .forEach(action => {
        const elem = document.querySelector('#' + action);
        elem.addEventListener('click', eval(action));
    });

firebase.auth()
    .onAuthStateChanged(updateAuthState);

