const firebaseConfig = {
    //your firebase config here
};
firebase.initializeApp(firebaseConfig);
let currentClickHandler;

function updateAuthState(user) {
    const pane = document.querySelector('#signInStatus');
    if (user) {
        pane.innerHTML = `
            <div class="profile">
              <img src="${user.photoURL || ''}" title="${user.displayName || 'No Name'}" />
              <p>${user.displayName || 'No Name'}</p>
              <button id="logout">Sign Out</button>
            </div>
            `;
        document.querySelector('#logout')
            .addEventListener('click', logout);
    } else {
        pane.innerHTML = `Not Signed In`;
        const logoutBtn = document.querySelector('#logout');
        if (logoutBtn) {
            logoutBtn.removeEventListener('click');
        }
    }
}

async function logout(ev) {
    ev.preventDefault();
    try {
        await firebase.auth().signOut();
        console.log('signed out');
    } catch (err) {
        alert(`Failed to logout: ${err.message}`);
        console.error('**', err);
    }
}

async function signUpByEmailAndPassword(ev) {
    ev.preventDefault();
    enableForm('Sign-Up', true, true, async (ev) => {
        ev.preventDefault();
        const email = document.querySelector('#email').value;
        const password = document.querySelector('#password').value;
        const name = document.querySelector('#name').value;
        if (email && password && name) {
            try {
                const result = await firebase.auth().createUserWithEmailAndPassword(email, password);
                const user = result.user;
                console.info('created user', user);

                await user.updateProfile({ displayName: name, photoURL: './img/user-male.png' });
                await user.sendEmailVerification();
                alert('Check your inbox for a verification email');
                disableForm();
            } catch (err) {
                alert(`${err.message} (${err.code})`);
            }
        }
    });
}

async function signInByEmailAndPassword(ev) {
    ev.preventDefault();
    enableForm('Sign-In', true, false, async (ev) => {
        ev.preventDefault();
        const email = document.querySelector('#email').value;
        const password = document.querySelector('#password').value;
        if (email && password) {
            try {
                const result = await firebase.auth().signInWithEmailAndPassword(email, password);
                const user = result.user;
                console.info('signed in user', user);
                disableForm();
            } catch (err) {
                alert(`${err.message} (${err.code})`);
            }
        }
    });
}

async function passwordReset(ev) {
    ev.preventDefault();
    enableForm('Send Reset Email', false, false, async (ev) => {
        ev.preventDefault();
        const email = document.querySelector('#email').value;
        if (email) {
            try {
                await firebase.auth().sendPasswordResetEmail(email);
                alert('Check your inbox for a reset email');
                disableForm();
            } catch (err) {
                alert(`${err.message} (${err.code})`);
            }
        }
    });
}

function enableForm(actionText, showPassword, showName, onClick) {
    const form = document.querySelector('#emailPasswordForm');
    form.querySelector('input[type="password"]').style.display = showPassword ? 'block' : 'none';
    form.querySelector('input[type="text"]').style.display = showName ? 'block' : 'none';
    form.querySelector('button').innerText = actionText;
    currentClickHandler = onClick;
    form.querySelector('button').addEventListener('click', currentClickHandler);
    form.style.opacity = 1;
}

function disableForm() {
    const form = document.querySelector('#emailPasswordForm');
    form.style.opacity = 0;
    form.querySelector('button').removeEventListener('click', currentClickHandler);
    currentClickHandler = undefined;
    form.querySelectorAll('input').forEach(field => field.value = '');
}

document.querySelector('#signUp')
    .addEventListener('click', signUpByEmailAndPassword);
document.querySelector('#signIn')
    .addEventListener('click', signInByEmailAndPassword);
document.querySelector('#resetPassword')
    .addEventListener('click', passwordReset);
firebase.auth()
    .onAuthStateChanged(updateAuthState);
