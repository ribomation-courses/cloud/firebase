const firebaseConfig = {
    //your firebase config here
};
firebase.initializeApp(firebaseConfig);
let unsubscribe;

function updateAuthState(user) {
    const pane = document.querySelector('#signInStatus');
    const tbl = document.querySelector('#tbl');
    if (user) {
        pane.innerHTML = `
            <div class="profile">
              <img src="${user.photoURL || ''}" title="${user.displayName || 'No Name'}" />
              <p>${user.displayName || 'No Name'}</p>
              <button id="logout">Sign Out</button>
            </div>
            `;
        document.querySelector('#logout')
            .addEventListener('click', logout);

        tbl.classList.remove('hidden');
        tbl.classList.add('visible');

        unsubscribe = firebase.firestore()
            .collection('persons')
            .onSnapshot(personsHandler);
    } else {
        pane.innerHTML = `Not Signed In`;
        const logoutBtn = document.querySelector('#logout');
        if (logoutBtn) {
            logoutBtn.removeEventListener('click', logout);
        }

        tbl.classList.remove('visible');
        tbl.classList.add('hidden');
        if (unsubscribe) {
            unsubscribe();
            unsubscribe = undefined;
        }
    }
}

async function logout(ev) {
    ev.preventDefault();
    try {
        await firebase.auth().signOut();
        console.log('signed out');
    } catch (err) {
        alert(`Failed to logout: ${err.message}`);
        console.error('**', err);
    }
}

async function signInByGoogle(ev) {
    ev.preventDefault();
    try {
        firebase.auth().useDeviceLanguage(); //or; .languageCode = 'sv';
        const provider = new firebase.auth.GoogleAuthProvider();
        provider.addScope('profile');
        provider.addScope('email');
        const result = await firebase.auth().signInWithPopup(provider);
        const user = result.user;
        console.info('signed in', user);
    } catch (err) {
        console.error(`*** ${err.message} (${err.code})`);
    }
}

async function personsHandler(snap) {
    const rows = document.querySelector('#tbl');
    rows.innerHTML = '';
    snap.forEach(doc => {
        let id = doc.id;
        let data = doc.data();
        console.log('**', id, ':', data);
        let tr = document.createElement('tr');
        tr.innerHTML = `
            <td>${id}</td>
            <td>${data.name}</td>
            <td>${data.age}</td>
            <td></td>
            `;
        rows.appendChild(tr);
    });
}

document.querySelector('#useGoogle')
    .addEventListener('click', signInByGoogle);

firebase.auth()
    .onAuthStateChanged(updateAuthState);


