# Firebase, 2 days
Welcome to this course.
The syllabus can be found at
[cloud/firebase](https://www.ribomation.se/courses/cloud/firebase.html)

Here you can find
* Installation instructions
* Solutions to the exercises

# Installation Instructions
In order to do the programming exercises of the course, 
you need to have the following software installed:

* [Node JS](https://nodejs.org/en/download/)
* [Microsoft Visual Code](https://code.visualstudio.com/) or
    * [JetBrains WebStorm (_30 days trial_)](https://www.jetbrains.com/webstorm/download)
* [Google Chrome](https://www.google.se/chrome/browser/desktop/)
* [GIT Client](https://git-scm.com/downloads)

During the course we will install additional software, such as the firebase-tools, using NPM.


# Google Account for Firebase
In order to interact with Google Firebase you need to have a Google Account. If you don't,
please sign-up before the course starts and
verify you can sign-in.
* https://firebase.google.com/


# Course GIT Repo
It's recommended that you keep the git repo and your solutions separated. 
Create a dedicated directory for this course and a sub-directory for 
each chapter. Get the course repo initially by a `git clone` operation

    mkdir -p ~/firebase-course/my-solutions
    cd ~/firebase-course
    git clone https://gitlab.com/ribomation-courses/cloud/firebase.git gitlab

Get any updates by a `git pull` operation

    cd ~/firebase-course/gitlab
    git pull



***
*If you have any questions, don't hesitate to contact me*<br>
**Jens Riboe**<br/>
Ribomation AB<br/>
[jens.riboe@ribomation.se](mailto:jens.riboe@ribomation.se)<br/>
[www.ribomation.se](https://www.ribomation.se)<br/>



