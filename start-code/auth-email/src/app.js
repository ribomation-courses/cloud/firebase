const firebaseConfig = {
    projectId: "...",
    apiKey: "...",
    authDomain: "...",
};
firebase.initializeApp(firebaseConfig);
let currentClickHandler;

function updateAuthState(user) {
    const pane = document.querySelector('#signInStatus');
    if (user) {
        pane.innerHTML = `
            <div class="profile">
              <img src="${user.photoURL || ''}" title="${user.displayName || 'No Name'}" />
              <p>${user.displayName || 'No Name'}</p>
              <button id="logout">Sign Out</button>
            </div>
            `;
        document.querySelector('#logout')
            .addEventListener('click', logout);
    } else {
        pane.innerHTML = `Not Signed In`;
        const logoutBtn = document.querySelector('#logout');
        if (logoutBtn) {
            logoutBtn.removeEventListener('click');
        }
    }
}

async function logout(ev) {
    //...
}

async function signUpByEmailAndPassword(ev) {
    ev.preventDefault();
    enableForm('Sign-Up', true, true, async (ev) => {
        ev.preventDefault();
        const email = document.querySelector('#email').value;
        const password = document.querySelector('#password').value;
        const name = document.querySelector('#name').value;
        if (email && password && name) {
            //...
        }
    });
}

async function signInByEmailAndPassword(ev) {
    ev.preventDefault();
    enableForm('Sign-In', true, false, async (ev) => {
        ev.preventDefault();
        const email = document.querySelector('#email').value;
        const password = document.querySelector('#password').value;
        if (email && password) {
            //...
        }
    });
}

async function passwordReset(ev) {
    ev.preventDefault();
    enableForm('Send Reset Email', false, false, async (ev) => {
        ev.preventDefault();
        const email = document.querySelector('#email').value;
        if (email) {
            //...
        }
    });
}

function enableForm(actionText, showPassword, showName, onClick) {
    const form = document.querySelector('#emailPasswordForm');
    form.querySelector('input[type="password"]').style.display = showPassword ? 'block' : 'none';
    form.querySelector('input[type="text"]').style.display = showName ? 'block' : 'none';
    form.querySelector('button').innerText = actionText;
    currentClickHandler = onClick;
    form.querySelector('button').addEventListener('click', currentClickHandler);
    form.style.opacity = 1;
}

function disableForm() {
    const form = document.querySelector('#emailPasswordForm');
    form.style.opacity = 0;
    form.querySelector('button').removeEventListener('click', currentClickHandler);
    currentClickHandler = undefined;
    form.querySelectorAll('input').forEach(field => field.value = '');
}

document.querySelector('#signUp')
    .addEventListener('click', signUpByEmailAndPassword);
document.querySelector('#signIn')
    .addEventListener('click', signInByEmailAndPassword);
document.querySelector('#resetPassword')
    .addEventListener('click', passwordReset);
firebase.auth()
    .onAuthStateChanged(updateAuthState);
