const firebaseConfig = {
    projectId: "...",
    apiKey: "...",
    authDomain: "...",
};
firebase.initializeApp(firebaseConfig);


function updateAuthState(user) {
    const pane = document.querySelector('#signInStatus');
    if (user) {
        pane.innerHTML = `
            <div class="profile">
              <img src="${user.photoURL || ''}" title="${user.displayName || 'No Name'}" />
              <p>${user.displayName || 'No Name'}</p>
              <button id="logout">Sign Out</button>
            </div>
            `;
        document.querySelector('#logout')
            .addEventListener('click', logout);
    } else {
        pane.innerHTML = `Not Signed In`;
        const logoutBtn = document.querySelector('#logout');
        if (logoutBtn) {
            logoutBtn.removeEventListener('click', logout);
        }
    }
}

async function logout(ev) {
    //...
}

async function signInByGoogle(ev) {
    //...
}

document.querySelector('#useGoogle')
    .addEventListener('click', signInByGoogle);
firebase.auth()
    .onAuthStateChanged(updateAuthState);
