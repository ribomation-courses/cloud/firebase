const firebaseConfig = {
    apiKey: "...",
    authDomain: "...",
    databaseURL: "...",
    projectId: "...",
    storageBucket: "...",
    messagingSenderId: "...",
    appId: "..."
};

firebase.initializeApp(firebaseConfig);
const db = firebase.firestore();
const personsCol = db.collection('persons');


async function createPerson(ev) {
    //...
}

async function deletePerson(ev) {
    //...
}

async function personsHandler(snap) {
    //...
}

personsCol.onSnapshot(personsHandler);
document.querySelector('#create').addEventListener('click', createPerson);

